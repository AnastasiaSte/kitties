import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
constructor() { }
ngOnInit (): void{
}
  title = 'kitties';
  breeds = [
  {id:0, name:"Persian", image: "assets/Kitties_images/PersianCat.jpg"},
  {id:1,name:"British", image: "assets/Kitties_images/BritishShorthair.jpg"},
  {id:2,name:"MaineCoonCat", image: "assets/Kitties_images/MaineCoon.jpg"},
  {id:3,name:"SiameseCat", image: "assets/Kitties_images/Siamese.jpg"},
  {id:4,name:"Scottish", image: "assets/Kitties_images/ScottishCat.jpg"},
  ];
  cats: any[] = [];
  onNewKitty (kitty: any){
  //this.cats = [];
  this.cats.push(kitty)
  console.log(this.cats);
  }
  }

